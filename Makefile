BUILD=build
EXECUTABLE=$(BUILD)/main

all: dircheck
	$(DIRCHECK)
	cd $(BUILD) && cmake -DBUILD=Release ..
	cd $(BUILD) && make

dircheck:
	`if [ ! -d $(BUILD) ]; then mkdir $(BUILD); fi`

verbose: dircheck
	cd $(BUILD) && cmake -DCMAKE_VERBOSE_MAKEFILE=1 ..
	cd $(BUILD) && make

run: debug
	./$(EXECUTABLE)

debug: dircheck
	cd $(BUILD) && cmake -DBUILD=Debug ..
	cd $(BUILD) && make

clean:
	rm -rf $(BUILD)
	cd win32 && make clean
	
cross:
	cd win32 && make

