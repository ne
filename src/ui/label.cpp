/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "label.h"
#include "common.h"

Label::Label(const std::string &name) : Element(name)
{
	ui_init();
}

void Label::draw()
{
	glPushMatrix();

	glDisable(GL_LIGHTING);

	glTranslatef(m_pos.x(), m_pos.y(), m_pos.z());
	glTranslatef(0,0,-0.1);
	//glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHTING);
	glColor3f(m_r, m_g, m_b);
	ui_draw_rect(m_size.x(), m_size.y());

	glTranslatef(0,0,0.1);
	//glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glColor3f(0,0,0);
	ui_draw_text(UIFont_Medium,m_name.c_str(),0,0,0);

	glPopMatrix();
}
