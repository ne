/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef QUAT_H_
#define QUAT_H_

#include <GL/gl.h>
#include <ode/ode.h>
#include <math.h>
#include "vec.h"

//namespace Base {

#define SLERP_DELTA 0.001

template<class T>
class Quat
{
public:
	Quat() : m_w(0.0) {}
	Quat(T w, const Vec<T> &v) : m_v(v), m_w(w) {}
	Quat(T w, T x, T y, T z) : m_v(x, y, z), m_w(w) {}

	///////////////////////
	// Set/Get functions //
	///////////////////////
	inline const Quat<T> set(T w, const Vec<T> &v)
	{
		m_v = v;
		m_w = w;
		return *this;
	}
	inline const Quat<T> set(T x, T y, T z, T w)
	{
		m_v = Vec<T>(x, y, z);
		m_w = w;
		return *this;
	}

	inline void get(T v[4]) const
	{
		v[0] = x();
		v[1] = y();
		v[2] = z();
		v[3] = w();
	}

	inline const Vec<T> v() const {return m_v;}
	inline T x() const {return m_v.x();}
	inline T y() const {return m_v.y();}
	inline T z() const {return m_v.z();}
	inline T w() const {return m_w;}

	////////////////
	// Conversion //
	////////////////
	inline T angle() const
	{
		return acos(w())*2.0;
	}

	inline static const Quat<T> FromAxis(T angle, const Vec<T> &v)
	{
		T a = angle * 0.5;

		return Quat<T>(cos(a), v * sin(a));
	}

	inline static const Quat<T> FromODE(const dReal *q)
	{
		return Quat<T>(q[0], q[1], q[2], q[3]);
	}

	inline const dReal *toODE()
	{
		static dReal q[4];
		q[0] = w();
		q[1] = x();
		q[2] = y();
		q[3] = z();
		return q;
	}

	inline void toMatrixGL(T m[16])
	{
		T x2 = x() + x();
		T y2 = y() + y();
		T z2 = z() + z();

		T xx = x() * x2;
		T xy = x() * y2;
		T xz = x() * z2;

		T yy = y() * y2;
		T yz = y() * z2;

		T zz = z() * z2;

		T wx = w() * x2;
		T wy = w() * y2;
		T wz = w() * z2;

		m[ 0] = 1.0 - (yy + zz);
		m[ 1] = xy + wz;
		m[ 2] = xz - wy;
		m[ 3] = 0.0;

		m[ 4] = xy - wz;
		m[ 5] = 1.0 - (xx + zz);
		m[ 6] = yz + wx;
		m[ 7] = 0.0;

		m[ 8] = xz + wy;
		m[ 9] = yz - wx;
		m[10] = 1.0 - (xx + yy);
		m[11] = 0.0;

		m[12] = 0.0;
		m[13] = 0.0;
		m[14] = 0.0;
		m[15] = 1.0;
	}

	inline const Vec<T> toEuler() const
	{
		T nx, ny, nz;
		double test = x() * y() + z() * w();

		if (test > 0.5 - SLERP_DELTA) {
			nx = 2.0 * atan2(x(), w());
			ny = M_PI * 0.5;
			nz = 0.0;
		}
		else if (test < -0.5 + SLERP_DELTA) {
			nx = -2.0 * atan2(x(), w());
			ny = -M_PI * 0.5;
			nz = 0.0;
		}
		else {
			T xx, yy, zz, x2, y2, z2, w2;

			x2 = x() * 2.0;
			y2 = y() * 2.0;
			z2 = z() * 2.0;
			w2 = w() * 2.0;

			xx = x2 * x();
			yy = y2 * y();
			zz = z2 * z();

			nx = atan2(y2 * w() - x2 * z(), 1.0 - yy - zz);
			ny = asin(2.0 * test);
			nz = atan2(x2 * w() - y2 * z(), 1.0 - xx - zz);
		}

		return Vec<T>(nx, ny, nz);
	}

	/////////////
	// 3D Math //
	/////////////

	inline T norm() const
	{
		return w() * w() + v().dot(v());
	}

	inline const Quat<T> unit() const
	{
		return Quat<T>(*this / norm());
	}

	inline const Quat<T> inverse() const
	{
		return conjugate() / norm();
	}

	inline const Quat<T> conjugate() const
	{
		return Quat<T>(w(), -v());
	}

	inline const Quat<T> operator * (const Quat<T> &q) const
	{
		//T w = m_w * q.m_w - m_v.dot(q.m_v);
		//Vec<T> v(m_v.cross(q.m_v) + m_w * q.m_v + q.m_w * m_v);
		//return Quat<T>(w, v);

		return Quat<T>(*this) *= q;
	}

	inline const Quat<T> &operator *= (const Quat<T> &q)
	{
		T a, b, c, d, e, f, g, h;
		a = (w() + x()) * (q.w() + q.x());
		b = (z() - y()) * (q.y() - q.z());
		c = (w() - x()) * (q.y() + q.z());
		d = (y() + z()) * (q.w() - q.x());
		e = (x() + z()) * (q.x() + q.y());
		f = (x() - z()) * (q.x() - q.y());
		g = (w() + y()) * (q.w() - q.z());
		h = (w() - y()) * (q.w() + q.z());

		T w = b + (-e - f + g + h) * 0.5;
		T x = a - ( e + f + g + h) * 0.5;
		T y = c + ( e - f + g - h) * 0.5;
		T z = d + ( e - f - g + h) * 0.5;

		m_w = w;
		m_v = Vec<T>(x, y, z);

		return *this;
	}

	inline const Quat<T> slerp(const Quat<T> &to, T t)
	{
		double om, cosom, sinom, s1, s2;

		Quat<T> tmp(to);

		cosom = v().dot(to.v()) + w() * to.w();

		if (cosom < 0.0) {
			cosom = -cosom;
			tmp.m_w = -tmp.m_w;
			tmp.m_v = -tmp.m_v;
		}

		if ((1.0 - cosom) > SLERP_DELTA) {
			om = acos(cosom);
			sinom = sin(om);
			s1 = sin((1.0 - t) * om) / sinom;
			s2 = sin(t * om) / sinom;
		}
		else {
			// do linear because they are very close
			s1 = 1.0 - t;
			s2 = t;
		}

		return Quat<T>(s1 * x() + s2 * tmp.x(),
					   s1 * y() + s2 * tmp.y(),
					   s1 * z() + s2 * tmp.z(),
					   s1 * w() + s2 * tmp.w());
	}

	///////////////////////
	// Equality overload //
	///////////////////////

	inline const Quat<T> &operator = (const Quat<T> &q)
	{
		m_w = q.m_w;
		m_v = q.m_v;
		return *this;
	}
	inline bool operator == (const Quat<T> &q) const
	{
		return m_w == q.m_w && m_v == q.m_v;
	}

	/////////////////////
	// Scalar overload //
	/////////////////////

	inline const Quat<T> operator / (T s) const
	{
		return Quat<T> (m_w / s, m_v / s);
	}

protected:
	Vec<T> m_v;
	T m_w;

};

typedef Quat<float> Quatf;
typedef Quat<double> Quatd;

//}

#endif /*QUAT_H_*/
