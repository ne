/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef UI_COMMON_H_
#define UI_COMMON_H_

#include <GL/gl.h>
#include <FTGL/ftgl.h>
/*#include <FTGL/FTFont.h>
#include <FTGL/FTGLExtrdFont.h>
#include <FTGL/FTGLPixmapFont.h>
#include <FTGL/FTGLTextureFont.h>*/

class Texture;

/**** FONTS ****/

#define UI_FONT_SCALETWEAK 0.025

#define UI_FONT "data/fonts/freesansbold.ttf"
#define UI_FONTSIZE_MEDIUM 32
#define UI_FONTSIZE_SMALL 16

extern FTGLTextureFont *UIFont_Medium, *UIFont_Small;

/**** TEXTURES ****/

#define UI_CORNER_SIZE 1
#define UI_CORNER "data/menu/corner.png"

extern Texture *UITex_Corner;
extern GLuint UIList_Corner;

/**** TOOLS ****/

void ui_init();

void ui_draw_rect(float x, float y);

void ui_draw_text(FTFont *f, const char *msg, float x, float y, float z);

#endif
