#include "time.h"

//using namespace Base;

#ifdef WIN32
    #include <windows.h>
#else
	#include <sys/time.h>
	#include <time.h>
#endif

Time::Stamp Time::Now()
{
	long time;
#ifdef WIN32
	time = timeGetTime();
#else
	static timeval tp;
	gettimeofday(&tp, 0);
	time = tp.tv_sec * 1000 + tp.tv_usec * 0.001;
#endif
	return time;
}


void Time::Sleep(unsigned int millisecs)
{
#ifdef WIN32
	Sleep(millisecs);
#else
	struct timespec req, rem;
	req.tv_sec = 0;
	req.tv_nsec = millisecs * 1000000;
	if (nanosleep(&req, &rem) == -1)
		nanosleep(&rem, 0);
#endif
}
