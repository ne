/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "texture.h"
#include "error.h"
#include "video.h"

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL_image.h>

/* Undefine the following if you're targeting
 * hardware that supports non-power-of-2 textures.
 * You'll save some video memory...
 */
//#define NONPOW2

#define FAIL_ON_GLMEM \
{ \
	if (glGetError() == GL_OUT_OF_MEMORY) { \
		fprintf(stderr, "Out of memory!"); \
		exit(1); \
	} \
}

void t_init()
{
	static int init = 0;
	if (init) return;
	init = 1;

	//glMatrixMode(GL_TEXTURE);
	//glLoadIdentity();
	//glScalef(-1,1,1);
}

struct texture *t_create()
{
	struct texture *t = (struct texture*)malloc(sizeof(struct texture));

	t->ready = 0;
	t->gl_handle = 0;

	return t;
}

void t_destroy(struct texture *t)
{
	t_unload(t);
	free(t);
}

int t_ready(struct texture *t)
{
	return t->ready;
}

static int nearest_pow2(int s)
{
	int v = 1;
	while (v < s) {
		v <<= 1;
	}
	return v;
}

static SDL_Surface *flip_fit_surf(SDL_Surface *surf)
{
#ifdef NONPOW2
	int w = surf->w;
	int h = surf->h;
#else
	int w = nearest_pow2(surf->w);
	int h = nearest_pow2(surf->h);
#endif

#ifdef DEBUG
	if (w != surf->w || h != surf->h) {
		printf("DEBUG: resizing surface from %dx%d to %dx%d for GL\n",
				surf->w, surf->h, w, h);
	}
#endif

	SDL_Surface *n = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
			0x000000FF,
			0x0000FF00,
			0x00FF0000,
			0xFF000000
#else
			0xFF000000,
			0x00FF0000,
			0x0000FF00,
			0x000000FF
#endif
			);

	if (!n) return NULL;

	Uint32 flags = surf->flags&(SDL_SRCALPHA|SDL_RLEACCELOK);
	Uint8 alpha = surf->format->alpha;

	// turn off alpha for the blit
	if (flags&SDL_SRCALPHA)
		SDL_SetAlpha(surf, 0, 0);

	SDL_Rect src, dest;
	src.x = dest.x = 0;
	src.y = 0;
	dest.y = h - surf->h; // copy to the bottom of the pow2 texture
	src.w = surf->w;
	src.h = surf->h;
	SDL_BlitSurface(surf, &src, n, &dest);

	// turn alpha back on if we turned it off
	if (flags&SDL_SRCALPHA)
		SDL_SetAlpha(surf, flags, alpha);

	// now, flip the image
	Uint8 *line = malloc(n->pitch * sizeof(Uint8));

	Uint8 *pix = (Uint8*)n->pixels;
	Uint16 pitch = n->pitch;

	SDL_LockSurface(n);
	int i;
	for (i=0; i < n->h/2; i++) {
		int end = n->h - i - 1;
		memcpy(line, pix + pitch*i, pitch); // save
		memcpy(pix + pitch*i, pix + pitch*end, pitch); // low to high
		memcpy(pix + pitch*end, line, pitch); // saved to low
	}
	SDL_UnlockSurface(n);

	free(line);

	return n;
}

void t_load(struct texture *t, const char *filename)
{
	t_unload(t);
	t->ready = 0;

	SDL_Surface *img = IMG_Load(filename);

	if (!img) {
		fprintf(stderr, "Texture load error: %s\n", IMG_GetError());
		return;
	}

	if (img->format->BytesPerPixel < 2) {
		fprintf(stderr, "Texture load error: %s is not true color\n", filename);
		return;
	}

	SDL_Surface *tmp = flip_fit_surf(img);

	glGenTextures(1, &t->gl_handle);
	glBindTexture(GL_TEXTURE_2D, t->gl_handle);

	t->width = img->w;
	t->height = img->h;

	t->u = (float)img->w / tmp->w;
	t->v = (float)img->h / tmp->h;

	SDL_FreeSurface(img);
	img = tmp;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	GLuint mode = GL_RGB;
	if (img->format->BytesPerPixel == 4) mode = GL_RGBA;

	int mipmap = 0;

	if (!mipmap) {
		glTexImage2D(GL_TEXTURE_2D, 0, img->format->BytesPerPixel,
					 img->w, img->h, 0, mode, GL_UNSIGNED_BYTE, img->pixels);
	}
	else {
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		gluBuild2DMipmaps(GL_TEXTURE_2D, img->format->BytesPerPixel, img->w, img->h,
						  mode, GL_UNSIGNED_BYTE, img->pixels);
	}

	SDL_FreeSurface(img);

	FAIL_ON_GLMEM

	t->ready = 1;
}

void t_unload(struct texture *t)
{
	if (t_ready(t)) {
		glDeleteTextures(1, &t->gl_handle);
	}
}

void t_apply(struct texture *t)
{
	GLint lastmode;
	glGetIntegerv(GL_MATRIX_MODE, &lastmode);
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	glScalef(t->u, t->v, 1.0);
	glBindTexture(GL_TEXTURE_2D, t->gl_handle);
	glMatrixMode(lastmode);
}

void t_clear()
{
	GLint lastmode;
	glGetIntegerv(GL_MATRIX_MODE, &lastmode);
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D, 0);
	glMatrixMode(lastmode);
}

void t_set(struct texture *t, GLuint name, GLuint value)
{
	t_apply(t);
	glTexParameteri(GL_TEXTURE_2D,name,value);
}

struct texture *t_screenshot()
{
	struct texture *t = t_create();

	glGenTextures(1, &t->gl_handle);
	glBindTexture(GL_TEXTURE_2D, t->gl_handle);

	const struct video_info* vi = v_info();

#ifdef NONPOW2
	int w = vi->width;
	int h = vi->height;
#else
	int w = nearest_pow2(vi->width);
	int h = nearest_pow2(vi->height);
#endif

	t->width = vi->width;
	t->height = vi->height;

#ifdef DEBUG
	if (w != vi->width || h != vi->height) {
		printf("DEBUG: resizing surface from %dx%d to %dx%d for GL\n",
				vi->width, vi->height, w, h);
	}
#endif

	t->u = (float)t->width / w;
	t->v = (float)t->height / h;

	glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, w, h, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	FAIL_ON_GLMEM

	t->ready = 1;

	return t;
}

