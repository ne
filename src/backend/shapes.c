/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "shapes.h"

#include <GL/gl.h>
#include <math.h>

static GLuint circle, quad, cube, sphere;

void shapes_init()
{
	static int init = 0;
	if (init) return;

	init = 1;

	quad = glGenLists(1);
	{
		glNewList(quad,GL_COMPILE);
		glBegin( GL_QUADS );
		glNormal3f(0.0,0.0,1.0);
		glTexCoord2f(1.0,1.0); glVertex2f( 0.5, 0.5);
		glTexCoord2f(0.0,1.0); glVertex2f(-0.5, 0.5);
		glTexCoord2f(0.0,0.0); glVertex2f(-0.5,-0.5);
		glTexCoord2f(1.0,0.0); glVertex2f( 0.5,-0.5);
		glEnd();
		glEndList();
	}

	circle = glGenLists(1);
	{
		glNewList(circle,GL_COMPILE);
	    int points = 64;
	  	glBegin(GL_TRIANGLE_FAN);
	  	glNormal3f(0.0f, 0.0f, 1.0f);
	  	int i;
	    for (i=0; i<points; i++)
	    {
	    	float t = (float)i/points;
	    	t *= 2 * M_PI;
	    	glVertex2f(0.5*cos(t),0.5*sin(t));
	    }
	    glEnd();
	    glEndList();
	}

	cube = glGenLists(1);
	{
		glNewList(cube,GL_COMPILE);
		glBegin(GL_QUADS);
		// front
		glNormal3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.5f,-0.5f, 0.5f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.5f,-0.5f, 0.5f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.5f, 0.5f, 0.5f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.5f, 0.5f, 0.5f);
		// back
		glNormal3f(0.0f, 0.0f,-1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5f,-0.5f,-0.5f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5f, 0.5f,-0.5f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.5f, 0.5f,-0.5f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.5f,-0.5f,-0.5f);
		// top
		glNormal3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.5f, 0.5f,-0.5f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.5f, 0.5f, 0.5f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.5f, 0.5f, 0.5f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.5f, 0.5f,-0.5f);
		// bottom
		glNormal3f(0.0f,-1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5f,-0.5f,-0.5f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.5f,-0.5f,-0.5f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.5f,-0.5f, 0.5f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5f,-0.5f, 0.5f);
		// right
		glNormal3f(1.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.5f,-0.5f,-0.5f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.5f, 0.5f,-0.5f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.5f, 0.5f, 0.5f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.5f,-0.5f, 0.5f);
		// left
		glNormal3f(-1.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.5f,-0.5f,-0.5f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5f,-0.5f, 0.5f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5f, 0.5f, 0.5f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.5f, 0.5f,-0.5f);
		glEnd();
		glEndList();
	}
}

void shapes_texturize(struct texture *tex, int tile)
{
	if (tex)
	{
		glEnable(GL_TEXTURE_2D);
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glScalef(tile,tile,tile);
		glMatrixMode(GL_MODELVIEW);
		t_apply(tex);
	}
	else
	{
		glDisable(GL_TEXTURE_2D);
	}
}

void draw_quad()
{
	glCallList(quad);
}

void draw_circle()
{
	glCallList(circle);
}

void draw_cube()
{
	glCallList(cube);
}

void draw_sphere()
{
	glCallList(sphere);
}

