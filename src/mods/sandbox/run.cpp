/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "backend/video.h"
#include "backend/input.h"
#include "backend/texture.h"
#include "backend/shapes.h"

#include "phys/camera.h"
#include "phys/world.h"
#include "phys/generic_objects.h"
#include "phys/player.h"
#include "phys/conversion.h"

#include "mods/mod.h"

#include "base/time.h"

#include "planet.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>

using namespace SandBox;

int sandbox_run(void *);

/* see mods/list.h for what this is for... */
struct mod sandbox_mod = {
	sandbox_run,
	"Sandbox Demo",
	"A demo for testing features"
};

int gl_init();

int sandbox_run(void *)
{
	Camera cam;

	//int width = v_info()->width;
	//int height = v_info()->height;

	gl_init();

	int *screen_pos;
	double *gl_pos;

	World *w = World::Instance();

	std::vector<Object*> objects;

	Texture home_tex("data/material/moon.jpg");

	Planet home(&home_tex, 128, 0.02);
	home.setStatic(true);

	Player player(5);
	//Sphere player(5, 1);
	float player_speed = 500;
	player.setPosition(0.0, 128.0 + 8, 0.0);
	objects.push_back(&player);

	//cam.follow(&player);
	cam.setOffset(Vec3f(0.0, 10.0, 10.0));
	cam.setSpeed(0.5);

	cam.setPosition(Vec3f(100.0, 0.0, 400.0));
	//cam.setPosition(Vec3f(0.0, 128.0, 100.0));
	//cam.lookAt(player.getPositionV());

	Time::Stamp delta = 0;
	Time::Stamp frame_time = Time::Now();

	int done = 0;
	while (!done)
	{
		frame_time = Time::Now();

		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		cam.apply();

		{
			Vec3f pos = player.getPositionV();
			glColor3f(1.0, 0.0, 0.0);
			player.getUp().draw(pos, 10);
			player.getForward().draw(pos, 10);
			glColor3f(0.0, 1.0, 0.0);
			pos.draw(pos, 30);
		}

		for (size_t i=0; i<objects.size(); i++) {
			objects[i]->clearGravity();
			home.applyGravity(objects[i]);
			objects[i]->update();
		}

		cam.update();

		i_pump();

		keysym_t *key = i_keystate();

		if (key[K_ESCAPE]) {
			//done = 1;
			exit(0);
		}

		if (key[K_LEFT]) {
			//player.getBody()->addRelForce(player_speed, 0.0, 0.0);
			player.getBody()->addTorque(0.0, player_speed * 0.1, 0.0);
		}
		if (key[K_RIGHT]) {
			//player.getBody()->addRelForce(-player_speed, 0.0, 0.0);
			player.getBody()->addTorque(0.0, -player_speed * 0.1, 0.0);
		}
		if (key[K_UP]) {
			player.getBody()->addRelForce(0.0, 0.0, player_speed);
		}
		if (key[K_DOWN]) {
			player.getBody()->addRelForce(0.0, 0.0, -player_speed);
		}

		w->step();

		screen_pos = i_mpos();
		gl_pos = v_unproject(screen_pos[0],screen_pos[1],1);

		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_LIGHT1);

		glDisable(GL_LIGHTING);

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		if (0) {
				glPushMatrix();
				glTranslatef(gl_pos[0],gl_pos[1],0);
				glColor3f(1,0,0);
				glBegin(GL_LINES);
				glVertex2f(-10,-10);
				glVertex2f(10,10);
				glVertex2f(-10,10);
				glVertex2f(10,-10);
				glEnd();
				glPopMatrix();
		}
		glEnable(GL_DEPTH_TEST);

		glEnable(GL_TEXTURE_2D);
		glColor3f(1.0, 1.0, 1.0);
		home.draw();

		glEnable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
		glColor3f(1.0, 0.0, 0.0);
		player.draw();
		static float rot = 0;
		rot += 0.1;

		v_flip();

		delta = Time::Now() - frame_time;
		if (20 > delta) {
			Time::Sleep(20 - delta);
		}
	}

	return 0;
	}

	int gl_init()
	{
	//glClearColor(0,0,0,0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	double w = v_info()->width, h = v_info()->height;
	double aspect = w/h;

	gluPerspective(45,aspect,1,3000);

	glEnable(GL_DEPTH_TEST);
	glClearDepth(1);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	glShadeModel(GL_SMOOTH);

	//glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}
