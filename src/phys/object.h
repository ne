/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef OBJECT_H_
#define OBJECT_H_

#include <ode/ode.h>
#include <ode/odecpp.h>
#include <ode/odecpp_collision.h>
#include <string>
#include "base/vec.h"

class Object
{
public:
	Object();
	virtual ~Object();

	void setCollide(int (*func)(Object*,Object*)) { m_colfunc = func; }
	virtual int collide(Object* o);

	virtual void update() {}

	virtual void draw() = 0;

	dBody *getBody() { return m_body; }
	dGeom *getGeom() { return m_geom; }
	float getMass() { return m_mass.mass; }

	inline const Vec3f getUp()
	{
		const dReal *r = getRotation();

		return Vec3f(r[1], r[5], r[9]);
	}

	inline const Vec3f getForward()
	{
		const dReal *r = getRotation();

		return Vec3f(r[2], r[6], r[10]);
	}

	void setPosition(dReal x, dReal y, dReal z);
	const dReal *getPosition();
	inline const Vec3f getPositionV()
	{
		const dReal *v = getPosition();
		return Vec3f(v[0], v[1], v[2]);
	}

	void setRotation(const dMatrix3 r);
	const dReal *getRotation();

	void setQuaternion(const dQuaternion q);
	const dReal *getQuaternion();

	bool isStatic() { return m_static; }
	void setStatic(bool is_static);

	virtual std::string &serialize();
	virtual void unserialize(std::string &s);

	// returns true when the gravity applied is the strongest
	virtual bool applyGravity(const Vec3f &force);

	inline void clearGravity()
	{
		m_gravity_mag = 0;
	}

protected:
	void apply_matrix();
	void init_geom();

	long int m_framestamp;
	Vec3f m_gravity;
	float m_gravity_mag;

	dMass m_mass;
	dBody *m_body;
	dGeom *m_geom;

private:
	bool m_static;
	int (*m_colfunc)(Object*,Object*);

	float m_spring, m_damp;
};

#endif
