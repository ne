/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "menu.h"
#include "resource/texturefactory.h"
#include "backend/video.h"
#include "backend/input.h"
#include "backend/shapes.h"
#include <stdlib.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include <math.h>

static void gl_perspective()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	double w = v_info()->width, h = v_info()->height;
	double aspect = w/h;

	gluPerspective(45,aspect,1,1001);

	glEnable(GL_DEPTH_TEST);
	glClearDepth(1);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	glShadeModel(GL_SMOOTH);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
}

Menu::Menu(std::string name, Menu::MENU_MODE m)
	: Element(name), m_background(0), m_retain_background(false), m_selected(0)
{
	m_keyrep.setDelay(150);
	m_keyrep.setTime(300);
	setMode(m);
	setSize(Vec3f(v_info()->width, v_info()->height, 9.0));
	setDefaults();
}

Menu::~Menu()
{
	delete m_background;
}

void Menu::setDefaults()
{
	m_entry_spacing = 2.0; // units step per entry
	m_entry_padding = 3.0; // units of padding around the entry
	m_rot_target = 0.0;
	m_rot_accel = 0.05;
	m_rot_vel = 0.0;
	m_rot_friction = 1.0 - 0.2;
	m_rot_curr = 0.0;
}

void Menu::addChild(Element *e)
{
	m_children.push_back(e);
}

int Menu::run()
{
	v_title(m_name.c_str(), m_name.c_str());
	v_resizecb(gl_perspective);

	if (m_mode == OVERLAY || m_mode == OVERLAY_ANIMATE) {
		// take snapshot of background to render later
		if (!m_retain_background || !m_background) {
			if (m_background)
				delete m_background;

			m_background = Texture::Screenshot();
		}
	}
	else {
		//
	}

	init_perspective();

	int ret = 0;
	bool done = false;
	while (!done) {
		int result = handleInput();
		if (result == -1) {
			ret = -1;
			done = true;
		}
		else if (result == 1) {
			v_resizecb(NULL);
			const char *name = m_children[m_selected]->getName().c_str();
			v_title(name, name);
			ret = m_children[m_selected]->run();
			i_clear();
			break;
		}

		simulate();
		draw();
		v_flip();
		SDL_Delay(10);
	}

	return ret;
}

int Menu::handleInput()
{
	i_pump();
	keysym_t *key = i_keystate();

	if (key[K_ESCAPE]) {
		//return -1;
	}
	else if (key[K_UP]) {
		if (m_selected != 0 && m_keyrep.repeat()) {
			m_selected--;
		}
	}
	else if (key[K_DOWN]) {
		if (m_selected+1 < (int)m_children.size() && m_keyrep.repeat()) {
			m_selected++;
		}
	}
	else if (key[K_RETURN]) {
		if (m_keyrep.repeat()) {
			return 1;
		}
	}
	else {
		m_keyrep.clear();
	}

	return 0;
}

void Menu::draw()
{
	static float brightness = 0.4;
	static float bg_col[] = { 0, 0, 0 };
	static int bg_last = rand()%3;
	static int bg_curr = -1;
	//bg_col[bg_last] = 1;

	if (m_mode == BACKGROUND) {
		if (bg_curr == -1) {
			bg_curr = bg_last;
			while (bg_last == bg_curr) bg_curr = rand()%3;
		}
		if (bg_col[bg_curr] >= 1) {
			bg_last = bg_curr;
			bg_curr = -1;
		} else {
			bg_col[bg_curr] += 0.01;
			bg_col[bg_last] -= 0.01;
		}
		glClearColor(bg_col[0]*brightness, bg_col[1]*brightness, bg_col[2]*brightness, 0);
	}

	int draw_start = m_selected-m_entry_padding;
	int draw_end = m_selected+m_entry_padding+1;
	int upper_bound = 0;
	int lower_bound = m_children.size()-1;

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	//glDisable(GL_LIGHTING);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	if (m_mode == OVERLAY || m_mode == OVERLAY_ANIMATE) {
		// draw snapshot
		glDisable(GL_DEPTH_TEST);

		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);

		glLoadIdentity();
		glScalef(2.0, 2.0, 1.0);
		glColor3f(1.0, 1.0, 1.0);
		glEnable(GL_TEXTURE_2D);
		m_background->apply();
		draw_quad();

		if (m_mode == OVERLAY_ANIMATE) {
			glLoadIdentity();

			float w, h, ratio;
			w = m_size.x();
			h = m_size.y();
			ratio = w/h;

			TextureFactory *tf = TextureFactory::Instance();
			static Texture *image = tf->load("data/ne/ne.png");

			float hx, hy;
			hx = image->getWidth()*0.5;
			hy = image->getHeight()*0.5;

			static float x= w*0.5 - hx;
			static float y= h*0.5 - hy;
			static float vx = 5.0;
			static float vy = 5.0;

			x += vx;
			y += vy;

			bool onscreen = true;
			if (x + hx < -w || x - hx > w)
				onscreen = false;
			if (y + hy < -h || y - hy > h)
				onscreen = false;

			if (!onscreen) {
				x = 0.0;
				y = 0.0;
			}

			if (x - hx < -w || x + hx > w)
				vx = -vx;
			if (y - hy < -h || y + hy > h)
				vy = -vy;

			glTranslatef(x/w, y/h, 0.0);
			glScalef(1.0/ratio, 1.0, 1.0);
			image->apply();
			draw_quad();
		}

		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
	}

	// draw triangles
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);

	glDisable(GL_TEXTURE_2D);
	glColor3f(1.0, 1.0, 1.0);

	glLoadIdentity();
	glBegin(GL_TRIANGLES);
	glVertex2f(-1.0,-0.1);
	glVertex2f(-0.9, 0.0);
	glVertex2f(-1.0, 0.1);
	glVertex2f( 1.0, 0.1);
	glVertex2f( 0.9, 0.0);
	glVertex2f( 1.0,-0.1);
	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	// end triangles

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -10.0);

	for (int i=draw_start; i!=draw_end; i++) {
		//float r = m_rot_curr - m_entry_spacing*i;
		float r = m_rot_curr - i * m_entry_spacing;

		glPushMatrix();
		glRotatef(r*10,1, 0.0, 0.0);
		glTranslatef(0.0, 0.0, -m_size.z());

		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_QUADS);
		//glColor4f(1,1,1,1);
		glVertex2f(0.0, 0.0);
		glVertex2f(0.0, 10.0);
		//glColor4f(1,1,1,0);
		glVertex2f(10.0, 10.0);
		glVertex2f(10.0, 0.0);
		glEnd();

		if (i >= upper_bound && i <= lower_bound)
			m_children[i]->draw();
		glPopMatrix();
	}
}

void Menu::simulate()
{
	m_rot_target = m_selected*m_entry_spacing;

	// smoothly scroll the list
	float delta = m_rot_target-m_rot_curr;

	m_rot_vel += m_rot_accel * delta;
	m_rot_curr += m_rot_vel;

	// apply friction
	m_rot_vel *= m_rot_friction;
}

void Menu::init_perspective()
{
	gl_perspective();
}

