/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef VEC_H_
#define VEC_H_

#include <GL/gl.h>
#include <ode/ode.h>

//namespace Base {

template<typename T>
class Vec
{
public:
	Vec(T v=0) : m_x(v), m_y(v), m_z(v) {}
	Vec(T x, T y, T z) : m_x(x), m_y(y), m_z(z) {}

	////////////////
	// Conversion //
	////////////////

	inline static const Vec<T> FromODE(const dReal *v)
	{
		return Vec<T>(v[0], v[1], v[2]);
	}

	///////////////////////
	// Set/Get functions //
	///////////////////////

	inline const Vec<T> set(T v)
	{
		m_x = m_y = m_z = v;
		return *this;
	}
	inline const Vec<T> set(T x, T y, T z)
	{
		m_x = x; m_y = y; m_z = z;
		return *this;
	}
	inline void get(T v[3]) const
	{
		v[0] = m_x;
		v[1] = m_y;
		v[2] = m_z;
	}

	inline T x() const { return m_x; }
	inline T y() const { return m_y; }
	inline T z() const { return m_z; }

	inline const Vec<T> operator - () const
	{
		return Vec<T>(-m_x, -m_y, -m_z);
	}

	///////////////////////
	// Equality overload //
	///////////////////////

	inline const Vec<T> operator = (const Vec<T> &q)
	{
		return set(q.m_x,q.m_y,q.m_z);
	}
	inline bool operator == (const Vec<T> &q)
	{
		return m_x==q.m_x && m_y==q.m_y && m_z==q.m_z;
	}

	/////////////////////
	// Vector overload //
	/////////////////////

	inline const Vec<T> operator + (const Vec<T> &v) const
	{
		return Vec<T>(m_x+v.m_x, m_y+v.m_y, m_z+v.m_z);
	}
	inline const Vec<T> operator += (const Vec<T> &v)
	{
		return set(m_x+v.m_x, m_y+v.m_y, m_z+v.m_z);
	}

	inline const Vec<T> operator - (const Vec<T> &v) const
	{
		return Vec<T>(m_x-v.m_x, m_y-v.m_y, m_z-v.m_z);
	}
	inline const Vec<T> operator -= (const Vec<T> &v)
	{
		return set(m_x-v.m_x, m_y-v.m_y, m_z-v.m_z);
	}

	inline const Vec<T> operator * (const Vec<T> &v) const
	{
		return Vec<T>(m_x*v.m_x, m_y*v.m_y, m_z*v.m_z);
	}
	inline const Vec<T> operator *= (const Vec<T> &v)
	{
		return set(m_x*v.m_x, m_y*v.m_y, m_z*v.m_z);
	}

	inline const Vec<T> operator / (const Vec<T> &v) const
	{
		return Vec<T>(m_x/v.m_x, m_y/v.m_y, m_z/v.m_z);
	}
	inline const Vec<T> operator /= (const Vec<T> &v)
	{
		return set(m_x/v.m_x, m_y/v.m_y, m_z/v.m_z);
	}

	/////////////////////
	// Scalar overload //
	/////////////////////

	inline const Vec<T> operator + (T v) const
	{
		return *this + Vec<T>(v);
	}
	inline const Vec<T> operator += (T v)
	{
		return *this += Vec<T>(v);
	}

	inline const Vec<T> operator - (T v) const
	{
		return *this - Vec<T>(v);
	}
	inline const Vec<T> &operator -= (T v)
	{
		return *this -= Vec<T>(v);
	}

	inline const Vec<T> operator * (T v) const
	{
		return *this * Vec<T>(v);
	}
	inline const Vec<T> operator *= (T v)
	{
		return *this *= Vec<T>(v);
	}

	inline const Vec<T> operator / (T v) const
	{
		return *this / Vec<T>(v);
	}
	inline const Vec<T> operator /= (T v)
	{
		return *this /= Vec<T>(v);
	}

	/////////////
	// 3D Math //
	/////////////

	inline T length() const
	{
		return sqrt(dot(*this));
	}

	inline const Vec<T> unit() const
	{
		return *this / (T)length();
	}

	inline T dot(const Vec<T> &v) const
	{
		return m_x*v.m_x + m_y*v.m_y + m_z*v.m_z;
	}

	inline const Vec<T> cross(const Vec<T> &v) const
	{
		return Vec<T>(m_y * v.m_z - m_z * v.m_y,
					  m_z * v.m_x - m_x * v.m_z,
					  m_x * v.m_y - m_y * v.m_x);
	}

	// returns a rotation matrix (for OpenGL) that
	// specifies a rotation of angle around vector v
	inline static void RotateGL(T angle, const Vec<T> &v, GLfloat *r)
	{
		GLfloat c = cos(angle);
		GLfloat s = sin(angle);
		GLfloat t = 1.0 - c;

		r[ 0] = t * v.x() * v.x() + c;
		r[ 1] = t * v.x() * v.y() + s * v.z();
		r[ 2] = t * v.x() * v.z() + s * v.y();
		r[ 3] = 0.0;

		r[ 4] = t * v.x() * v.y() - s * v.z();
		r[ 5] = t * v.y() * v.y() + c;
		r[ 6] = t * v.y() * v.z() + s * v.x();
		r[ 7] = 0.0;

		r[ 8] = t * v.x() * v.z() + s * v.y();
		r[ 9] = t * v.y() * v.z() + s * v.x();
		r[10] = t * v.z() * v.z() + c;
		r[11] = 0.0;

		r[12] = 0.0;
		r[13] = 0.0;
		r[14] = 0.0;
		r[15] = 1.0;
	}

	inline const Vec<T> rotate(T angle, const Vec<T> &v)
	{
		GLfloat r[16];
		RotateGL(angle, v, r);
		return rotate(r);
	}

	// rotate via an OpenGL matrix
	inline const Vec<T> rotate(const T *r)
	{
		return Vec<T>(m_x*r[0] + m_y*r[4] + m_z*r[8],
					  m_x*r[1] + m_y*r[5] + m_z*r[9],
					  m_x*r[2] + m_y*r[6] + m_z*r[10]);
	}

	///////////////
	// Debugging //
	///////////////

	void draw(const Vec<T> &pos, GLfloat scale = 1.0) const
	{
		glPushMatrix();
		glTranslatef(pos.x(), pos.y(), pos.z());
		glScalef(scale, scale, scale);
		draw();
		glPopMatrix();
	}

	void draw() const
	{
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		//glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		glVertex3f(x(), y(), z());
		glVertex3f(0, 0, 0);
		glEnd();
		glPopAttrib();
	}

protected:
	T m_x, m_y, m_z;
};

typedef Vec<float> Vec3f;
typedef Vec<double> Vec3d;

//}

#endif /*VEC_H_*/

