/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef FLAT_H_
#define FLAT_H_

#include "generic_objects.h"
#include "backend/texture.h"

class Flat : public Box
{
public:
	Flat(float x, float y, float density=1);
	Flat(Texture *t, float scale, float density=1);
	virtual ~Flat();

	virtual void draw();
	
	Texture *getTexture() { return m_tex; }
	void setTexture(Texture *t) { m_tex = t; }
	
protected:
	Texture *m_tex;
	float m_x, m_y;
	
};

#endif /*FLAT_H_*/
