/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <GL/gl.h>

#ifdef __cplusplus
#include <string>
extern "C" {
#endif

void t_init();

struct texture
{
	int ready;
	int width, height;
	GLfloat u, v;
	GLuint gl_handle;
};

struct texture *t_create();

void t_destroy(struct texture *t);

int t_ready(struct texture *t);

void t_load(struct texture *t, const char *filename);

void t_unload(struct texture *t);

void t_apply(struct texture *t);

void t_clear();

void t_set(struct texture *t, GLuint name, GLuint value);

struct texture *t_screenshot();

/* C++ Texture */
#ifdef __cplusplus
}

class Texture
{
public:
	Texture(const char *file = 0, bool mipmap=true)
	{
		t_init();
		m_tex = t_create();

		if (file)
			load(file);
	}
	Texture(GLuint texid)
	{
		t_init();
		m_tex = t_create();
		m_tex->gl_handle = texid;
		m_tex->ready = 1;
	}
	~Texture()
	{
		t_destroy(m_tex);
	}

	static Texture *Screenshot()
	{
		Texture *t = new Texture();
		t->m_tex = t_screenshot();
		return t;
	}

	inline int getWidth()
	{
		return m_tex->width;
	}

	inline int getHeight()
	{
		return m_tex->height;
	}

	inline float getU()
	{
		return m_tex->u;
	}

	inline float getV()
	{
		return m_tex->v;
	}

	inline GLuint getHandle()
	{
		return m_tex->gl_handle;
	}

	inline bool isReady()
	{
		return (m_tex && t_ready(m_tex));
	}

	inline void set(GLuint name, GLuint value)
	{
		t_set(m_tex,name,value);
	}

	inline void load(const char *file, bool mipmap=true)
	{
		t_load(m_tex,file);
	}

	inline void unload()
	{
		t_unload(m_tex);
	}

	inline void apply()
	{
		t_apply(m_tex);
	}

	inline void clear()
	{
		t_clear();
	}

private:
	struct texture *m_tex;
};

#endif

#endif /*TEXTURE_H_*/
