/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "texturefactory.h"
#include <stdlib.h>
#include <stdio.h>

TextureFactory* TextureFactory::Instance()
{
	static TextureFactory tf;
	return &tf;
}

void TextureFactory::clear()
{
	std::map<std::string,Texture*>::iterator i;
	for (i = m_textures.begin(); i != m_textures.end(); i++)
	{
		delete (*i).second;
	}
}

Texture* TextureFactory::load(std::string file)
{
	Texture *t = m_textures[file];
	if (!t)
	{
		t = new Texture(file.c_str());
		if (!t->isReady())
		{
			fprintf(stderr,"Could not find texture '%s'\n", file.c_str());
			exit(1);
		}
		m_textures[file] = t;
	}
	return t;
}


