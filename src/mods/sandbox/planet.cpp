/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "planet.h"

using namespace SandBox;

Planet::Planet(Texture *texture, float radius, float density) : Sphere(radius,density), m_tex(texture)
{

}

Planet::~Planet()
{
}

void Planet::draw()
{
	m_tex->apply();
	Sphere::draw();
}

const Vec3f Planet::getGravity(Object *target)
{
	static float v[4];

	Vec3f pos1 = getPositionV();

	Vec3f pos2 = Vec3f::FromODE(target->getPosition());

	Vec3f del = pos1 - pos2;

	float r2 = del.dot(del);

	float n = 1.0 / sqrt(r2);

	Vec3f dir = del * n;

	dReal m1 = getMass();
	dReal m2 = target->getMass();

	dReal g = m1 * m2 / r2;

	return Vec3f(dir * g);
}

void Planet::applyGravity(Object *target)
{
	if (!target->getBody()->isEnabled()) return;

	Vec3f grav = getGravity(target);

	target->applyGravity(grav);
}
