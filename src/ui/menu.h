/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef MENU_H_
#define MENU_H_

#include <vector>
#include "backend/texture.h"
#include "element.h"
#include "keyrepeat.h"

class Menu : public Element
{
public:
	enum MENU_MODE {
		BACKGROUND,
		OVERLAY,
		OVERLAY_ANIMATE
	};

	Menu(std::string name, MENU_MODE m=BACKGROUND);
	~Menu();

	void addChild(Element *e);

	int run();
	void draw();

	inline void setMode(MENU_MODE m)
	{
		m_mode = m;
	}

	inline MENU_MODE getMode()
	{
		return m_mode;
	}

	inline void setRetainBackground(bool retain)
	{
		m_retain_background = retain;
	}

	void setDefaults();

private:
	int handleInput();
	void simulate();
	void init_perspective();

	KeyRepeat m_keyrep;

	Texture *m_background;
	bool m_retain_background;

	enum MENU_MODE m_mode;

	int m_selected;

	float m_entry_spacing;
	int m_entry_padding;
	float m_entry_distance;
	float m_rot_target;
	float m_rot_accel;
	float m_rot_vel;
	float m_rot_curr;
	float m_rot_friction;
};

#endif
