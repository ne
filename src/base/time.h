#ifndef TIMEUTIL_H
#define TIMEUTIL_H

//using namespace Base {

class Time
{
public:
    typedef unsigned long Stamp;

    static Stamp Now();

    static void Sleep(unsigned int millisecs);
};

//}

#endif // TIMEUTIL_H
