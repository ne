/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "planet.h"

using namespace Space;

Planet::Planet(Texture *texture, float radius, float density) : Sphere(radius,density), m_tex(texture)
{

}

Planet::~Planet()
{
}

void Planet::draw()
{
	m_tex->apply();
	Sphere::draw();
}

const float *Planet::getGravity(Object *target)
{
	static float v[4];

	const dReal *c = getPosition();

	const dReal *p = target->getPosition();

	dVector3 d = {c[0]-p[0], c[1]-p[1], c[2]-p[2]};
	dReal r2 = d[0]*d[0]+d[1]*d[1]+d[2]*d[2];
	dReal n = 1/sqrt(r2);

	dVector3 dir = {d[0]*n,d[1]*n,d[2]*n};

	dReal mp = getMass();
	dReal mo = target->getMass();

	dReal g = mp*mo/r2;

	v[0] = g*dir[0];
	v[1] = g*dir[1];
	v[2] = g*dir[2];
	v[3] = g;

	return v;
}

void Planet::applyGravity(Object *target)
{
	if (!target->getBody()->isEnabled()) return;

	const float *g = getGravity(target);

	target->getBody()->addForce(g[0],g[1],g[2]);
}
