/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "generic_objects.h"

#include "backend/shapes.h"
#include "world.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <ode/ode.h>

/*************
 * Box class *
 *************/

Box::Box(float x, float y, float z, float density)
{
	m_box = new dBox(World::Instance()->getSpace()->id(),x,y,z);
	m_geom = m_box;

	m_mass.setBox(density, x, y, z);
	m_body->setMass(&m_mass);

	shapes_init();
	init_geom();
}

Box::~Box()
{
	delete m_box;
}

void Box::draw()
{
	glPushMatrix();
	apply_matrix();
	static dReal s[3];
	m_box->getLengths(s);
	glScalef(s[0],s[1],s[2]);
	//draw_quad();
	draw_cube();
	//glColor3f(1,1,1);
	//glutWireCube(1);
	glPopMatrix();
}

/****************
 * Sphere class *
 ****************/

Sphere::Sphere(float r, float density)
{
	m_sphere = new dSphere(World::Instance()->getSpace()->id(),r);
	m_geom = m_sphere;

	m_mass.setSphere(density, r);
	m_body->setMass(&m_mass);

	init_geom();
}

Sphere::~Sphere()
{
	delete m_sphere;
}

void Sphere::draw()
{
	glPushMatrix();
	apply_matrix();
	static dReal s;
	s = m_sphere->getRadius();
	glScalef(s,s,s);
	//draw_circle();
	//glutWireSphere(1,64,64);
	static GLUquadricObj *q = 0;
	if (q == 0)
	{
		q = gluNewQuadric();
		gluQuadricNormals(q, GL_SMOOTH);
		gluQuadricTexture(q, GL_TRUE);
		glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
		glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	}
	glRotatef(-90,1,0,0);
	gluSphere(q,1,32,32);
	/*
	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);
	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	*/
	glPopMatrix();
}
