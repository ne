/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <map>

//namespace Base {

class Controller
{
public:
    Controller();

    enum ACTION
    {
        NOTHING,

        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT,

        LOOK,
        LOOK_UP,
        LOOK_DOWN,

        TURN,
        TURN_LEFT,
        TURN_RIGHT,

        JUMP,
        SELECT,
        CANCEL,
        MENU,

        LAST_ACTION
    };

    enum DIGITAL_EVENT
    {
        KEYBOARD,
        MOUSE,
        LAST_DIGITAL
    };

    enum ANALOG_EVENT
    {
        MOUSE_X,
        MOUSE_Y,
        LAST_ANALOG
    };

    void update();

    inline void bindDigital(enum DIGITAL_EVENT event,
                            unsigned int id, enum ACTION action)
    {
        digitalMap[event][id] = action;
    }

    inline void bindAnalog(enum ANALOG_EVENT event,
                           enum ACTION action)
    {
        analogMap[event] = action;
    }

    inline void eventDigital(enum DIGITAL_EVENT event,
                             unsigned int id, bool value)
    {
        enum ACTION a = digitalMap[event][id];
        digital[a] = value;
    }

    inline void eventAnalog(enum ANALOG_EVENT event, float value)
    {
        enum ACTION a = analogMap[event];
        analog[a] = value;
    }

    inline bool getDigital(enum ACTION action)
    {
        return digital[action];
    }

    inline float getAnalog(enum ACTION action)
    {
        return analog[action];
    }

private:
    bool digital[LAST_ACTION];
    std::map<unsigned int, enum ACTION> digitalMap[LAST_DIGITAL];

    float analog[LAST_ACTION];
    enum ACTION analogMap[LAST_ANALOG];
};

//}

#endif // CONTROLLER_H
