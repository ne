/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef PLANET_H_
#define PLANET_H_

#include "phys/generic_objects.h"
#include "backend/texture.h"

namespace SandBox {

class Planet : public Sphere
{
public:
	Planet(Texture *texture, float radius, float density=1);
	virtual ~Planet();

	void draw();

	// gets gravity as <x,y,z,g> where g is magnitude
	const Vec3f getGravity(Object *target);
	void applyGravity(Object *target);

private:
	Texture *m_tex;
};

}

#endif /*PLANET_H_*/
