/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef WORLD_H_
#define WORLD_H_

#include <ode/ode.h>
#include <ode/odecpp.h>
#include <ode/odecpp_collision.h>

class World
{
	World();
	~World();

	public:
	static World* Instance();

	void setStepCount(int count) { m_stepcount = count; }
	inline int getStepCount() { return m_stepcount; }

	void setStepSize(float size) { m_stepsize = size; }
	inline float getStepSize() { return m_stepsize; }

	void step();
	dReal trace(dRay *ray);

	dWorld *getWorld() { return &m_world; }
	dSpace *getSpace() { return m_space; }
	dJointGroup *getJointGroup() { return m_jointgroup; }

	private:
	int m_maxcontacts;
	int m_stepcount;
	float m_stepsize;

	dJointGroup *m_jointgroup;
	dSpace *m_space;
	dWorld m_world;
};

#endif
