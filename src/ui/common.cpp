/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "common.h"
#include "resource/texturefactory.h"
#include "backend/texture.h"

Texture *UITex_Corner;
GLuint UIList_Corner;

FTGLTextureFont *UIFont_Medium, *UIFont_Small;

static void build_box()
{
	UIList_Corner = glGenLists(1);

	float s = UI_CORNER_SIZE * 0.5;

	glNewList(UIList_Corner, GL_COMPILE);
	glBegin(GL_QUADS);
	glNormal3f(0,0,1);
	glTexCoord2i(0,0);
	glVertex2f(-s,-s);
	glTexCoord2i(1,0);
	glVertex2f( s,-s);
	glTexCoord2i(1,1);
	glVertex2f( s, s);
	glTexCoord2i(0,1);
	glVertex2f(-s, s);
	glEnd();
	glEndList();
}

void ui_init()
{
	static int init = 0;
	if (init) return;

	init = 1;
	TextureFactory *tf = TextureFactory::Instance();
	UITex_Corner = tf->load(UI_CORNER);
	UITex_Corner->apply();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	build_box();

	UIFont_Medium = new FTGLTextureFont(UI_FONT);
	UIFont_Medium->FaceSize(UI_FONTSIZE_MEDIUM);

	UIFont_Small = new FTGLTextureFont(UI_FONT);
	UIFont_Small->FaceSize(UI_FONTSIZE_SMALL);
}

void ui_draw_rect(float x, float y)
{
	float half = UI_CORNER_SIZE * 0.5;

	float left = -x * 0.5;
	float right = -left;
	float top = -y * 0.5;
	float bottom = -top;

	float scale = UI_CORNER_SIZE;

	glEnable(GL_TEXTURE_2D);

	glDisable(GL_DEPTH_TEST);

	UITex_Corner->apply();
	glEnable(GL_BLEND);

	// upper left
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(left+half, bottom-half, 0);
	glScalef(scale,scale,scale);
	glCallList(UIList_Corner);
	glPopMatrix();

	// lower left
	glPushMatrix();
	glMatrixMode(GL_TEXTURE);
	glTranslatef(0.5,0.5,0.0);
	glRotatef(-90,0,0,1);
	glTranslatef(-0.5,-0.5,0.0);
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(left+half, top+half, 0);
	glScalef(scale,scale,scale);
	glCallList(UIList_Corner);
	glPopMatrix();

	// lower right
	glPushMatrix();
	glMatrixMode(GL_TEXTURE);
	glTranslatef(0.5,0.5,0.0);
	glRotatef(-90,0,0,1);
	glTranslatef(-0.5,-0.5,0.0);
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(right-half, top+half, 0);
	glScalef(scale,scale,scale);
	glCallList(UIList_Corner);
	glPopMatrix();

	// upper right
	glPushMatrix();
	glMatrixMode(GL_TEXTURE);
	glTranslatef(0.5,0.5,0.0);
	glRotatef(-90,0,0,1);
	glTranslatef(-0.5,-0.5,0.0);
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(right-half, bottom-half, 0);
	glScalef(scale,scale,scale);
	glCallList(UIList_Corner);
	glPopMatrix();

	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);

	glBindTexture(GL_TEXTURE_2D, 0);

	// middle
	glPushMatrix();
	glScalef(x,y-1.9*scale,1);
	glCallList(UIList_Corner);
	glPopMatrix();

	// middle
	glPushMatrix();
	glScalef(x-1.9*scale,y,1);
	glCallList(UIList_Corner);
	glPopMatrix();

	glEnable(GL_DEPTH_TEST);

}

void ui_draw_text(FTFont *f, const char *msg, float x, float y, float z)
{
	FTPoint upper,lower;
	float cx, cy;
	float lx,ly,lz, ux,uy,uz;

	f->BBox(msg,lx,ly,lz,ux,uy,uz);
	cx = (ux-lx)*0.5;
	cy = (uy-ly)*0.5;

	cx *= UI_FONT_SCALETWEAK;
	cy *= UI_FONT_SCALETWEAK;

	glPushMatrix();
	glTranslatef(x-cx,y-cy,z);
	glScalef(UI_FONT_SCALETWEAK,UI_FONT_SCALETWEAK,UI_FONT_SCALETWEAK);
	f->Render(msg);
	glPopMatrix();
}
