/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "backend/input.h"
#include "backend/video.h"
#include "backend/shapes.h"
#include "mods/list.h"
#include "ui/menu.h"
#include "ui/label.h"
#include "ui/toggle.h"

#include <time.h>
#include <iostream>
#include <sstream>
#include <string>
#include <stdlib.h>

#define KEEPOVERLAY 0xDEADBEEF

int exit_func(void*) {
	return -1;
}

int print_toggle(void *data)
{
	Toggle *t =(Toggle*)data;
	std::cout << "Toggle '" << t->getName() << "' has a value of "
		<< t->getValue() << std::endl;

	return KEEPOVERLAY;
}

int menu_toggle(void *data)
{
	Menu *menu = (Menu*)data;

	if (menu->getMode() == Menu::OVERLAY || menu->getMode() == Menu::BACKGROUND)
		menu->setMode(Menu::OVERLAY_ANIMATE);
	else
		menu->setMode(Menu::OVERLAY);

	return KEEPOVERLAY;
}

int fullscreen_toggle(void *data)
{
	Menu *menu = (Menu*)data;

	v_fullscreen(1);

	Vec3f old = menu->getSize();
	menu->setSize(Vec3f(v_info()->width, v_info()->height, old.z()));

	return KEEPOVERLAY;
}

int main(int argc, char **argv)
{
	v_init();
	i_init();

	int width = 1024;
	int height = 768;
	int full = 0;

	// parse args
	{
		const char *usage = "USAGE:\n\t%s [-w WIDTH] [-h HEIGHT] [-f]\n";

		std::stringstream ss;
		for (int i=1; i<argc; i++)
			ss << argv[i] << " ";

		std::string arg_s;
		while (ss >> arg_s) {
			if (arg_s == "-w")
				ss >> width;
			else if (arg_s == "-h")
				ss >> height;
			else if (arg_s == "-f")
				full = 1;
			else {
				printf(usage, argv[0]);
				return 1;
			}
		}
	}

	srand(time(0));

	v_setup(width, height, full);

	shapes_init();

	Menu main_menu("Main Menu");

	Vec3f button_size(15.0, 2.5, 1.0);

	int mod_count = 0;

	for (struct mod* curr_mod = mod_list; curr_mod->runfunc != 0; curr_mod++) {
		Label *l = new Label(curr_mod->name);
		l->setCallback(curr_mod->runfunc);
		l->setSize(button_size);
		float c = 1.0 - 0.25 * (mod_count%2);
		l->setColor(c,c,1);
		main_menu.addChild(l);
		mod_count++;
	}

	Toggle *t = new Toggle("Menu Animation");
	t->setCallback(menu_toggle, &main_menu);
	t->setSize(button_size);
	t->setColor(0.6, 1.0, 0.6);
	main_menu.addChild(t);

	t = new Toggle("Fullscreen", full);
	t->setCallback(fullscreen_toggle, &main_menu);
	t->setSize(button_size);
	t->setColor(0.6, 1.0, 0.6);
	main_menu.addChild(t);

	Label *l = new Label("Quit");
	l->setCallback(exit_func);
	l->setSize(button_size);
	l->setColor(1.0, 0.0, 0.0);
	main_menu.addChild(l);

	std::cout << "Counted " << mod_count << " mods" << std::endl;

	Menu::MENU_MODE lastmode = Menu::BACKGROUND;

	int returned = KEEPOVERLAY;
	while (returned != -1) {

		if (returned == (int)KEEPOVERLAY) {
			main_menu.setRetainBackground(true);
			if (lastmode == Menu::BACKGROUND)
				lastmode = Menu::OVERLAY;
			else
				lastmode = main_menu.getMode();
		}
		else {
			main_menu.setRetainBackground(false);
			main_menu.setMode(lastmode);
		}

		returned = main_menu.run();
	}
	std::cout << "Exiting normally" << std::endl;

	return 0;
}

