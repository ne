/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef ELEMENT_H_
#define ELEMENT_H_

#include "resource/texturefactory.h"
#include "base/vec.h"
#include <vector>
#include <string>

class Element
{
public:
	Element(const std::string &name)
		: m_name(name), m_callback(0), m_r(1.0), m_g(1.0), m_b(1.0) {}
	virtual ~Element() {}

	virtual void draw() = 0;

	virtual int run()
	{
		if (m_callback)
			return m_callback(m_callback_data);
		else
			return 0;
	}

	inline void setCallback(int (*cb)(void*), void *data=0)
	{
		m_callback_data = data;
		m_callback = cb;
	}

	virtual void addChild(Element *e)
	{
		m_children.push_back(e);
	}

	inline unsigned int numChildren() const
	{
		return m_children.size();
	}

	inline Element *child(unsigned int index)
	{
		return m_children[index];
	}

	inline void setPos(Vec3f p)
	{
		m_pos = p;
	}

	inline const Vec3f &getPos() const
	{
		return m_pos;
	}

	inline void setSize(Vec3f s)
	{
		m_size = s;
	}

	inline const Vec3f &getSize() const
	{
		return m_size;
	}

	inline const std::string &getName() const
	{
		return m_name;
	}

	inline void setColor(float r, float g, float b)
	{
		m_r = r;
		m_g = g;
		m_b = b;
	}

protected:
	void drawBackround() {}

	std::string m_name;

	void *m_callback_data;
	int (*m_callback)(void*);

	Vec3f m_pos;
	Vec3f m_size;

	float m_r, m_g, m_b;

	std::vector<Element*> m_children;
};

#endif
