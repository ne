/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "world.h"

#include "base/vec.h"
#include "object.h"
#include <GL/gl.h>

/**********************
 * Collision callback *
 **********************/

#define MAX_CONTACTS 3
void collision_callback(void *data, dGeomID g1, dGeomID g2)
{
	if (dGeomIsSpace (g1) || dGeomIsSpace (g2)) {

		// colliding a space with something :
		dSpaceCollide2 (g1, g2, data,&collision_callback);

		// collide all geoms internal to the space(s)
		if (dGeomIsSpace (g1))
			dSpaceCollide ((dSpaceID)g1, data, &collision_callback);
		if (dGeomIsSpace (g2))
			dSpaceCollide ((dSpaceID)g2, data, &collision_callback);

		return;
	}

	dWorld *world = World::Instance()->getWorld();
	dJointGroup *jgroup = World::Instance()->getJointGroup();

	dContact contacts[MAX_CONTACTS];

	Object *o1 = (Object*)dGeomGetData(g1);
	Object *o2 = (Object*)dGeomGetData(g2);

	int num = dCollide(g1,g2,MAX_CONTACTS,&contacts[0].geom,sizeof(dContact));

	if (num && o1 && o2)
	{
		if ( o1->collide(o2) && o2->collide(o1) )
		{
			// they collide
		}
		else
		{
			// they don't
			return;
		}
	}

	dWorldID world_id = world->id();
	dJointGroupID jgroup_id = jgroup->id();

	for (int i=0; i<num; i++)
	{
		contacts[i].surface.mode = dContactBounce;
		contacts[i].surface.bounce = 0.0;//0.1;
		contacts[i].surface.bounce_vel = 0.0;//0.01;
		contacts[i].surface.mu = dInfinity;
	}

	for (int i=0; i<num; i++)
	{
		dJointID c = dJointCreateContact(world_id,jgroup_id,&contacts[i]);
		dJointAttach(c,dGeomGetBody(contacts[i].geom.g1),
					 dGeomGetBody(contacts[i].geom.g2));
	}
}

/***************
 * World class *
 ***************/

World::World() : m_maxcontacts(512), m_stepcount(2), m_stepsize(0.1)
{
	m_jointgroup = new dJointGroup(m_maxcontacts);
	m_space = new dHashSpace(0);
	dReal center[] = {0,0,0};
	dReal extents[] = {100,100,10};
	m_space = new dQuadTreeSpace(0,center,extents,6);
	dInitODE();
}

World::~World()
{
	delete m_jointgroup;
    delete m_space;
}

World* World::Instance()
{
	static World w;
	return &w;
}

void World::step()
{
	m_space->collide(0,collision_callback);

	for (int i=0; i<m_stepcount; i++)
	{
		dWorldQuickStep(m_world.id(),m_stepsize/m_stepcount);
	}

	m_jointgroup->empty();
}

void ray_callback(void *data, dGeomID g1, dGeomID g2)
{
	dContactGeom *res = (dContactGeom*)data;

	//Object *o1 = (Object*)dGeomGetData(g1);
	//Object *o2 = (Object*)dGeomGetData(g2);

	if (dGeomGetClass(g1) != dRayClass &&
		dGeomGetClass(g2) != dRayClass) {
		return;
	}

	dCollide(g1, g2, 1, res, sizeof(dContactGeom));

	/*
	printf("Got %d contacts\n", num);

	if (num) {
		printf("Found one\n");
		if (geom.depth <= res->depth) {
			*res = geom;
			printf("HIT\n");
		}
	}
	*/
}

dReal World::trace(dRay *ray)
{
	dContactGeom geom;
	geom.depth = dInfinity;
	ray->collide2((dGeomID)m_space->id(), &geom, ray_callback);
	return geom.depth;
}
