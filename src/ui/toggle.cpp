/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "toggle.h"
#include "common.h"
#include "backend/shapes.h"

Toggle::Toggle(const std::string &name, bool value)
	: Element(name), m_value(value)
{
	ui_init();
	shapes_init();
}

void Toggle::draw()
{
	glPushMatrix();

	glDisable(GL_LIGHTING);

	glTranslatef(m_pos.x(), m_pos.y(), m_pos.z());
	glTranslatef(0.0, 0.0, -0.1);
	//glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHTING);
	glColor3f(m_r, m_g, m_b);
	ui_draw_rect(m_size.x(), m_size.y());

	glTranslatef(0.0, 0.0, 0.1);
	//glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glColor3f(0.0, 0.0, 0.0);
	ui_draw_text(UIFont_Medium, m_name.c_str(), 0.0, 0.0, 0.0);

	glTranslatef(-m_size.x()*0.5 + 1.0, 0.0, 0.1);

	glDisable(GL_TEXTURE_2D);
	draw_circle();
	if (m_value)
		glColor3f(0.0, 1.0, 0.0);
	else
		glColor3f(1.0, 0.0, 0.0);
	glTranslatef(0.0, 0.0, 0.2 - m_value * 0.1);
	glScalef(0.75, 0.75, 0.75);
	draw_circle();

	glPopMatrix();
}

int Toggle::run()
{
	m_value = !m_value;
	return Element::run();
}
