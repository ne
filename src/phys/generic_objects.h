/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef GENERIC_OBJECTS_H_
#define GENERIC_OBJECTS_H_

#include "object.h"

class Box : public Object
{
public:
	Box(float x, float y, float z, float density);
	virtual ~Box();

	virtual void draw();

private:
	dBox *m_box;
};

class Sphere : public Object
{
public:
	Sphere(float r, float density);
	virtual ~Sphere();

	void draw();

private:
	dSphere *m_sphere;
};

#endif
