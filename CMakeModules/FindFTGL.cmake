# Locate ftgl
# This module defines
# FTGL_LIBRARY
# FTGL_FOUND, if false, do not try to link to ftgl
# FTGL_INCLUDE_DIR, where to find the headers
#
# $FTGL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$FTGL_DIR
#
# Created by Matt Thompson.
# Based on FindODE.cmake by David Guthrie and Robert Osfield 

FIND_PATH(FTGL_INCLUDE_DIR ode/ode.h
    ${FTGL_DIR}/include
    $ENV{FTGL_DIR}/include
    $ENV{FTGL_DIR}
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local/include
    /usr/include
    /sw/include # Fink
    /opt/local/include # DarwinPorts
    /opt/csw/include # Blastwave
    /opt/include
    [HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session\ Manager\\Environment;OSG_ROOT]/include
    /usr/freeware/include
)

MACRO(FIND_FTGL_LIBRARY MYLIBRARY MYLIBRARYNAME)

FIND_LIBRARY(${MYLIBRARY}
    NAMES ${MYLIBRARYNAME}
    PATHS
    ${FTGL_DIR}/lib
    $ENV{FTGL_DIR}/lib
    $ENV{FTGL_DIR}
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local/lib
    /usr/lib
    /sw/lib
    /opt/local/lib
    /opt/csw/lib
    /opt/lib
    [HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session\ Manager\\Environment;OSG_ROOT]/lib
    /usr/freeware/lib64
)

ENDMACRO(FIND_FTGL_LIBRARY MYLIBRARY MYLIBRARYNAME)

FIND_FTGL_LIBRARY(FTGL_LIBRARY ftgl)
#FIND_FTGL_LIBRARY(FTGL_LIBRARY_DEBUG ftgl)

SET(FTGL_FOUND "NO")
IF(FTGL_LIBRARY AND FTGL_INCLUDE_DIR)
    SET(FTGL_FOUND "YES")
ENDIF(FTGL_LIBRARY AND FTGL_INCLUDE_DIR)


