/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "backend/video.h"
#include "backend/input.h"
#include "backend/texture.h"
//#include "backend/text.h"

#include "phys/camera.h"
#include "phys/world.h"
#include "phys/generic_objects.h"
#include "phys/flat.h"

#include "mods/mod.h"

#include "planet.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <vector>

using namespace Space;

int space_run(void *);

/* see mods/list.h for what this is for... */
struct mod space_mod = {
	space_run,
	"Space Demo",
	"A simple demo with planets and satellites flying about"
};

static GLuint make_stars(int,int);
static int gl_init();

void show_message(const char *message)
{
	static int duration = 2000;
	static int time;
	static const char *m = 0;
	if (message) {
		time = SDL_GetTicks();
		m = message;
	}
	else if (m) {
		int now = SDL_GetTicks();
		glPushMatrix();
		glLoadIdentity();
		float alpha = 1 - (float)(now-time)/(float)duration;
		glColor4f(1,1,1,alpha);
		//text_draw(-1,0.75,-2,SIMPLE,m);
		glPopMatrix();
		if (now-time > duration) m = 0;
	}
}

int space_run(void *)
{
	Camera cam;

	int width = v_info()->width;
	int height = v_info()->height;

	Texture sat_tex("data/sprites/satellite.png");
	sat_tex.set(GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	Texture moon_tex("data/material/moon.jpg");
	Texture earth_tex("data/material/earth.jpg");

	float scale = 30;

	if (!sat_tex.isReady() || !moon_tex.isReady() || !earth_tex.isReady()) {
		return 1;
	}

	float speed = 1;

	double zoom = 0.35;
	float x = 0, y = 0, _x, _y;

	int *pos=0;
	double lpos[2] = {0,0};
	double *rpos=lpos;

	gl_init();
	GLuint star_list = make_stars(width,height);

	World *w = World::Instance();

	//dWorldSetAutoDisableLinearThreshold(w->getWorld()->id(),0.25);
	//dWorldSetAutoDisableAngularThreshold(w->getWorld()->id(),0.25);
	//dWorldSetAutoDisableFlag(w->getWorld->id(),1);

	std::vector<Planet*> planets;

	Planet *earth = new Planet(&earth_tex,63.71,.055);
	earth->setStatic(true);
	planets.push_back(earth);

	Planet *moon = new Planet(&moon_tex,17.37,5*.034);
	moon->setPosition(150,150,0);
	moon->setStatic(true);
	planets.push_back(moon);

	std::vector<Object*> objects;
	Object *current_object = 0, *last_object = 0;
	bool will_follow = false;

	std::vector<Object*> particles;

	int done = 0;
	while (!done)
	{
		////////////////
		// Frame init //
		////////////////

		// this stuff is for the fading background...
		{
			static float d = 0.0005;
			static float c = 0.0;
			glClearColor(c,0,c,0);
			c += d;
			if (c >= 0.2 || c <= 0.0) {
				d = -d;
				c += d;
			}
		}

		w->step();

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		i_pump();

		keysym_t *key = i_keystate();

		if (key[K_ESCAPE]) {
			done = 1;
		}

		if (key[K_RIGHT]) x -= speed;
		if (key[K_LEFT])  x += speed;
		if (key[K_UP])    y += speed;
		if (key[K_DOWN])  y -= speed;

		pos = i_mpos();

		///////////////////////
		// Camera transforms //
		///////////////////////

		if (i_mbutton(M_RIGHT)) {
			int *rpos = i_mrelpos();
			zoom += (float)rpos[1] * zoom*0.01;
		}

		if (zoom > 1.0) zoom = 1.0;
		if (zoom < 0.1) zoom = 0.1;

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glTranslatef(0,0,-zoom*1000);

		rpos = v_unproject(pos[0],pos[1],1);
		rpos[0] *= zoom;
		rpos[1] *= zoom;

		rpos[0] -= x;
		rpos[1] += y;

		cam.lookAt(Vec3f(-x,y,-10));
		cam.setPosition(Vec3f(-x,y,0));
		cam.update();
		cam.apply();
		//glScalef(zoom,zoom,zoom);

		///////////
		// Logic //
		///////////

		static int dropping = 0;
		static int following = 0;

		static bool operating_camera = false;
		if (key[K_SPACE] && !operating_camera) {
			will_follow = !will_follow;
			if (will_follow)
				show_message("Object tracking activated!");
			else
				show_message("Object tracking deactivated!");
			operating_camera = true;
			if (following) // go back to origin
			{
				following = 0;
				cam.follow(0);
				x = _x;
				y = _y;
			}
			else if (last_object) // go back to object
			{
				following = 1;
				cam.follow(last_object);
			_x = x;
			_y = y;
			x = y = 0;
			}
		}
		else if (!key[K_SPACE]) {
			operating_camera = false;
		}

		if (i_mbutton(M_MIDDLE) || key[K_SPACE]) {
			int *rpos = i_mrelpos();
			x += rpos[0]*zoom;
			y += rpos[1]*zoom;
		}
		else if (i_mbutton(M_LEFT)) {
			if (following && dropping == 0) {
				cam.follow(0);
				following = 0;
				x = _x;
				y = _y;
			}
			else if (dropping == 0) {
					// ready
					float r = (float)(rand()%10+4)/scale;
					float x = (float)sat_tex.getWidth()*r;
					float y = (float)sat_tex.getHeight()*r;
					Flat *flat = new Flat(x,y);
					flat->setTexture(&sat_tex);
					current_object = flat;
					current_object->setPosition(rpos[0],rpos[1],0);
					dropping = 2;
					lpos[0] = rpos[0];
					lpos[1] = rpos[1];
					cam.follow(0);
			}
			else if (dropping == 2) {
					// aim
					glDisable(GL_TEXTURE_2D);
					glBegin(GL_LINES);
					glColor3f(1,1,0);
					glVertex2f(lpos[0],lpos[1]);
					glColor3f(1,0,0);
					glVertex2f(rpos[0],rpos[1]);
					glEnd();
					current_object->setPosition(rpos[0],rpos[1],0);
			}
		}
		else if (dropping == 2) {
			// fire
			float cx = rpos[0]-lpos[0];
			float cy = rpos[1]-lpos[1];
			float n = 1;// / sqrt(x*x + y*y);
			float pow = 25*current_object->getMass();
			current_object->getBody()->addForce(-n*cx*pow,-n*cy*pow,0);
			objects.push_back(current_object);

			if (will_follow)
			{
				cam.follow(current_object);

				following = 1;
				_x = x;
				_y = y;
				x = y = 0;
			}

			last_object = current_object;
			current_object = 0;
			dropping = 3;
		}
		else {
			dropping = 0;
		}

		////////////////////
		// Physics update //
		////////////////////

		earth->update();
		for (size_t i=0; i<objects.size(); i++) {
			for (size_t j=0; j<planets.size(); j++)
				planets[j]->applyGravity(objects[i]);
			objects[i]->update();
		}

		/////////////
		// Drawing //
		/////////////

		glCallList(star_list);

		static float deg = 0;
		deg += 5;

		/*
		glPushMatrix();
		//glColor3f(0,1,0);
		//glScalef(radius,radius,1);
		//draw_circle();
		glRotatef(deg,1,-1,1);
		glutWireSphere(radius,64,64);
		//glutSolidSphere(radius,64,64);
		glPopMatrix();
		*/

		glEnable(GL_TEXTURE_2D);
		glColor3f(1,1,1);

		static float rotation = 0;
		rotation += 0.1;

		glPushMatrix();
		glRotatef(rotation,0,1,0);
		earth->draw();
		glPopMatrix();

		moon->draw();

		if (current_object) {
			current_object->draw();
		}

		for (size_t i=0; i<objects.size(); i++) {
			objects[i]->draw();
		}

		/******************
		 * Render the GUI *
		 ******************/
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		if (!following) {
			glPushMatrix();
			glTranslatef(rpos[0],rpos[1],0);
			glColor3f(1,0,0);
			glBegin(GL_LINES);
			glVertex2f(-10,-10);
			glVertex2f(10,10);
			glVertex2f(-10,10);
			glVertex2f(10,-10);
			glEnd();
			glPopMatrix();
		}
		show_message(0); // this will update the text if it was set
		glEnable(GL_DEPTH_TEST);

		SDL_Delay(10);

		v_flip();
	}

	for (size_t i=0; i<objects.size(); i++) {
		delete objects[i];
	}

	return 0;
}

GLuint make_stars(int width, int height)
{
	GLuint star_list = glGenLists(1);
    glNewList(star_list,GL_COMPILE);

    float n = width*height/1000;
    glDisable(GL_TEXTURE_2D);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(0,0,-1000);
    glScalef(2,2,2);
    glBegin(GL_POINTS);
    glColor3f(0.3,0.3,0.3);
    for (int i=0; i<n; i++)
    	glVertex2f((float)(rand()%width)-width/2,(float)(rand()%height)-height/2);
    glColor3f(0.6,0.6,0.6);
    for (int i=0; i<n; i++)
    	glVertex2f((float)(rand()%width)-width/2,(float)(rand()%height)-height/2);
    glColor3f(1.0,1.0,1.0);
    for (int i=0; i<n; i++)
    	glVertex2f((float)(rand()%width)-width/2,(float)(rand()%height)-height/2);
    glEnd();
    glPopMatrix();

    glEndList();
    return star_list;
}

int gl_init()
{
	//glClearColor(0,0,0,0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	double w = v_info()->width, h = v_info()->height;
	double aspect = w/h;

	gluPerspective(45,aspect,1,1001);

	glEnable(GL_DEPTH_TEST);
	glClearDepth(1);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	glShadeModel(GL_SMOOTH);

	//glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}
