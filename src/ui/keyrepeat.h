/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef KEYREPEAT_H_
#define KEYREPEAT_H_

class KeyRepeat
{
public:
	KeyRepeat() : m_delay(200), m_time(500), m_last(0) {}

	void clear() { m_last = 0; }
	bool repeat();

	void setDelay(int d) { m_delay = d; } // how fast keys repeat
	void setTime(int t) { m_time = t; } // how long it takes to start repeating

private:
	int m_delay, m_time, m_last;
};

#endif
