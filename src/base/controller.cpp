/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "controller.h"
#include <string.h>

#include "backend/input.h"

//using namespace Base;

Controller::Controller()
{
    memset(digital, 0, sizeof digital);

    memset(analog, 0, sizeof analog);
    memset(analogMap, 0, sizeof analogMap);
}

void Controller::update()
{
}
