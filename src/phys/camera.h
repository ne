/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef CAMERA_H_
#define CAMERA_H_

#include "base/vec.h"
#include "base/quat.h"

class Object;

class Camera
{
public:

	Camera();
	virtual ~Camera();

	void update();
	void apply();

	void follow(Object *o);

	inline void setOffset(const Vec3f &offset)
	{
		m_offset = offset;
	}

	inline void setUp(const Vec3f &up)
	{
		m_up = up;
	}

	inline void setSpeed(float speed)
	{
		m_speed = speed;
	}

	inline void setPosition(const Vec3f &pos)
	{
		m_pos = pos;
	}

	inline void lookAt(const Vec3f &target)
	{
		m_target = target;
	}

private:
	const Vec3f getUp();

	Object *m_object;

	float m_speed;
	Vec3f m_up, m_pos, m_offset, m_target;
	Vec3f m_cpos, m_ctarget;

	Quatf m_cquat;
};

#endif /*CAMERA_H_*/
