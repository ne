/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "player.h"

#include "world.h"
#include "backend/shapes.h"
#include "base/quat.h"

#include <ode/ode.h>
#include <GL/gl.h>
#include <GL/glu.h>

Player::Player(float height, float density) : m_standing(false)
{
	float radius, length;
	radius = height * 0.618;
	length = height * 0.85;

	World *w = World::Instance();
	m_capsule = new dCapsule(w->getSpace()->id(), radius, length);
	m_geom = m_capsule;

	m_ray = new dRay(0, getBodyHeight() - length);
	m_ray->setParams(0, 1);
	m_ray->setClosestHit(1);

	m_mass.setCapsule(density, 2, radius, length);
	m_body->setMass(&m_mass);

	init_geom();
	shapes_init();
}

Player::~Player()
{
	delete m_capsule;
	delete m_ray;
}

void Player::draw()
{
	glPushMatrix();
	apply_matrix();

	dReal radius, length;
	m_capsule->getParams(&radius, &length);

	if (m_standing)
		glColor3f(0,0,1);
	else
		glColor3f(1,1,1);

	glScalef(radius, length, radius);
	draw_cube();
	glPopMatrix();

	static GLUquadricObj *q = 0;
	if (q == 0)
	{
		q = gluNewQuadric();
		gluQuadricNormals(q, GL_SMOOTH);
		gluQuadricTexture(q, GL_TRUE);
		glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
		glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	}

	float hr = radius * 0.5;

	glPushMatrix();
	apply_matrix();
	glTranslatef(0.0, length*0.5, 0.0);
	glScalef(hr, hr, hr);
	gluSphere(q,1,16,16);
	glPopMatrix();

	glPushMatrix();
	apply_matrix();
	glTranslatef(0.0, -length*0.5, 0.0);
	glScalef(hr, hr, hr);
	gluSphere(q,1,16,16);
	glPopMatrix();

	if (m_standing)
		glColor3f(0,1,0);
	else
		glColor3f(1,1,0);

	Vec3f a = getBottom();
	Vec3f b = getFeet();

	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glVertex3f(a.x(), a.y(), a.z());
	glVertex3f(b.x(), b.y(), b.z());
	glEnd();
	glEnable(GL_LIGHTING);
}

void Player::update()
{
	update_ray();

	World *w = World::Instance();

	dReal depth = w->trace(m_ray);

	if (depth != dInfinity) {
		m_standing = true;
		m_depth = depth;

		Vec3f g = -m_gravity;

		printf("%.2f, %.2f, %.2f\n", g.x(), g.y(), g.z());

		Quatf to = Quatf(m_body->getQuaternion()[0], -m_gravity.unit());
		printf("%.2f, %.2f, %.2f, %.2f\n", to.w(), to.x(), to.y(), to.z());
		to = to.unit();
		printf("%.2f, %.2f, %.2f, %.2f\n", to.w(), to.x(), to.y(), to.z());

		to.v().draw(getPositionV(), 100);

		m_body->setQuaternion(to.toODE());
		m_body->setAngularVel(0.0, 0.0, 0.0);
		m_body->setTorque(0.0, 0.0, 0.0);

		dReal x = m_ray->getLength() - m_depth;

		dReal k = m_mass.mass * 2;

		Vec3f vel = Vec3f::FromODE(m_body->getLinearVel());
		vel *= w->getStepSize() * w->getStepCount();

		dReal vdamp = vel.dot(-getUp());

		dReal force = k * x + k * vdamp;

		printf("------------------\n");
		printf("x = %.2f    k = %.2f\n", x, k);
		printf("applying %.2f force\n", force);

		m_body->addRelForce(0.0, force, 0.0);

		//m_body->addTorque(ang_damp.x(), ang_damp.y(), ang_damp.z());

		// do damping
		dReal damp = -0.25;

		vel *= damp;
		//m_body->addForce(vel.x(), vel.y(), vel.z());

		//Vec3f tor = Vec3f::FromODE(m_body->getTorque()) * damp;

		//m_body->addTorque(tor.x(), tor.y(), tor.z());
	}
	else {
		Vec3f norm = getUp().unit();
		Vec3f grav = m_gravity.unit();

		float tip = 1.0 - norm.dot(grav);

		Quatf from = Quatf::FromODE(getQuaternion());

		Quatf to = Quatf::FromAxis(from.angle(), grav);
		Quatf q = from.slerp(to, 0.5);

		Vec3f eu = q.toEuler() * tip;
		getBody()->addTorque(eu.x(), eu.y(), eu.z());
	}
}

float Player::getBodyHeight()
{
	dReal length, radius;
	m_capsule->getParams(&radius, &length);

	return length + 2 * radius;
}

const Vec3f Player::getFeet()
{
	dReal dist = m_ray->getLength();
	Quatf quat = Quatf::FromODE(getQuaternion());

	return getBottom() + getUp() * -dist;
}

const Vec3f Player::getBottom()
{
	Vec3f pos = getPositionV();
	Quatf quat = Quatf::FromODE(getQuaternion());

	dReal radius, length;
	m_capsule->getParams(&radius, &length);

	length *= 0.5;

	Vec3f offset = getUp() * -length;

	return pos + offset;
}

void Player::update_ray()
{
	Vec3f bottom = getBottom();
	Vec3f down = -getUp();

	m_ray->set(bottom.x(), bottom.y(), bottom.z(),
			   down.x(), down.y(), down.z());
}

