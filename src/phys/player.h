/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef PLAYER_H_
#define PLAYER_H_

#include "object.h"
#include "base/vec.h"

class Player: public Object {
public:
	Player(float height, float density=1);
	virtual ~Player();

	virtual void draw();
	virtual void update();

	const Vec3f getFeet();
	const Vec3f getBottom();

	float getBodyHeight();

	inline bool isStanding() {return m_standing;}

protected:
	void update_ray();

	dCapsule *m_capsule;
	dRay *m_ray;

private:
	bool m_standing;
	dReal m_depth;
};

#endif /* PLAYER_H_ */
