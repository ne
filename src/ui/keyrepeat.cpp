/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "keyrepeat.h"

#include <SDL/SDL.h> // TODO: write a proper timer system

bool KeyRepeat::repeat()
{
	int now = SDL_GetTicks();
	if (m_last == 0) // a new key has been pressed
	{
		m_last = now;
		return true;
	}
	else if (now-m_last > m_time) // a key has been held long enough
	{
		m_last += m_delay;
		return true;
	}
	else
	{
		return false;
	}
}

