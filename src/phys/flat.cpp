/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "flat.h"

#include "backend/shapes.h"

void pin_constrain(Object *o)
{	
	const dReal *p = o->getPosition();
	o->setPosition(p[0],p[1],0);
	
	const dReal *q = o->getQuaternion();
	dReal n = 1;//sqrt(q[0]+q[3]);
	dVector4 q2 = {q[0]/n,0,0,q[3]/n};
	o->setQuaternion(q2);
}

Flat::Flat(float x, float y, float density) 
	: Box(x,y,1,density), m_tex(0), m_x(x), m_y(y)
{
	shapes_init(); // it's smart enough not to init twice...
}

Flat::Flat(Texture *t, float scale, float density)
	: Box(t->getWidth()*scale,t->getHeight()*scale,1,density), m_tex(t),
	  m_x(t->getWidth()*scale), m_y(t->getHeight()*scale)
{
}

Flat::~Flat()
{
}

void Flat::draw()
{
	pin_constrain(this);
	
	glPushMatrix();
	apply_matrix();
	
	glScalef(m_x,m_y,1);
	
	if (m_tex)
		m_tex->apply();
	draw_quad();
	
	glPopMatrix();
}
