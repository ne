/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "camera.h"

#include "base/vec.h"
#include "base/quat.h"
#include "object.h"
#include <math.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>

Camera::Camera() : m_object(0), m_speed(0.5)
{
	setUp(Vec3f(0.0, 1.0, 0.0));
}

Camera::~Camera()
{
}

void Camera::follow(Object *o)
{
	m_object = o;

	if (m_object) {
		const dReal *q = m_object->getRotation();
		m_cquat = Quatf(q[0], q[1], q[2], q[3]).inverse();
	}
}

void Camera::update()
{
	Vec3f delta;
	float dist;

	if (m_object) {
		const dReal *t = m_object->getPosition();

		m_target = Vec3f(t[0], t[1], t[2]);

		//Vec3f v = m_target + m_offset - m_cpos;
		//m_cpos += v * m_speed;

		m_cpos = m_target + m_object->getForward()*-10;
		//+ m_offset;

		m_ctarget = m_target;

		m_up = m_object->getUp();
	}
	else {
		delta = m_pos - m_cpos;
		dist = delta.dot(delta);
		if (dist != 0) {
			delta /= dist;
			dist *= m_speed;

			m_cpos += delta * dist;
		}

		delta = m_target - m_ctarget;
		dist = delta.dot(delta);
		if (dist != 0) {
			delta /= dist;
			dist *= m_speed;

			m_ctarget += delta * dist;
		}
	}
}

void Camera::apply()
{
		//GLfloat m[16];

		//const dReal *r = m_object->getRotation();
		//const dReal *t = m_object->getPosition();


		// the ODE rotation matrix is already transposed from opengl's viewpoint
		/*
		m[0] =r[0];
		m[1] =r[1];
		m[2] =r[2];
		m[3] =0;

		m[4] =r[4];
		m[5] =r[5];
		m[6] =r[6];
		m[7] =0;

		m[8] =r[8];
		m[9] =r[9];
		m[10]=r[10];
		m[11]=0;

		m[12]=0;
		m[13]=0;
		m[14]=0;
		m[15]=1;
		*/

		//glMultMatrixf (m);

	if (m_object) {
		const dReal *oq = m_object->getQuaternion();
		const dReal *op = m_object->getPosition();

		Quatf q(oq[0], oq[1], oq[2], oq[3]);
		q = q.conjugate();

		//Quatf d = m_cquat.slerp(q, 0.1);

		m_cquat = q;//.conjugate();

		GLfloat m[16];
		//m_cquat.toMatrixGL(m);
		m_cquat.toMatrixGL(m);

		glMultMatrixf(m);

		glTranslatef(-op[0],
					 -op[1],
					 -op[2]);

		//gluLookAt()

		//Vec3f off = m_offset * 100;
		//glTranslatef(-m_offset.x(), -m_offset.y(), -m_offset.z());
		//glTranslatef(-off.x(), -off.y(), -off.z());
	}
	else {
		gluLookAt(m_cpos.x(),    m_cpos.y(),    m_cpos.z(),
				  m_ctarget.x(), m_ctarget.y(), m_ctarget.z(),
				  m_up.z(),      m_up.y(),      m_up.z());
	}

		//printf("%f.2 %f.2 %f.2\n", m_cpos.x(), m_cpos.y(), m_cpos.z());
}


