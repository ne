/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef INPUT_H_
#define INPUT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "keysyms.h"
#include <stdint.h>

void i_init();

void i_pump();

void i_clear();

typedef uint8_t keysym_t;

keysym_t* i_keystate();

#define M_BUTTON(X)   (1 << ((X)-1))
#define M_LEFT         1
#define M_MIDDLE       2
#define M_RIGHT        3
#define M_WHEEL_UP     4
#define M_WHEEL_DOWN   5

struct input_mouse
{
    int x, y;
    int rel_x, rel_y;
    int button;
    int wheel;
};

const struct input_mouse* i_mouse();

int i_mbutton(int button);

int *i_mpos();

int *i_mrelpos();

int i_mwheel();

#ifdef __cplusplus
}
#endif

#endif
