/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "video.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_opengl.h>

static struct video_info vinfo;

void (*resize_cb)() = NULL;

void set_vinfo(int width, int height, int bpp, int flags, int fullscreen)
{
    vinfo.width = width;
    vinfo.height = height;
    vinfo.bpp = bpp;
    vinfo.flags = flags;
    vinfo.fullscreen = fullscreen;
}

const struct video_info* v_info()
{
    return (const struct video_info*)&vinfo;
}

static int closable = 1;

void v_closable(int isclosable)
{
	closable = isclosable;
}

void apply_resize()
{
	int w = vinfo.width, h = vinfo.height;
	SDL_SetVideoMode(w, h, vinfo.bpp, vinfo.flags);
	glViewport(0, 0, w, h);
	if (resize_cb)
		resize_cb();
}

int event_filter(const SDL_Event *e)
{
	if (e->type == SDL_QUIT && closable)
	{
		exit(0);
	}
	else if (e->type == SDL_VIDEORESIZE)
	{
		vinfo.width = e->resize.w;
		vinfo.height = e->resize.h;
		apply_resize();
		return 0;
	}
	else
	{
		return 1;
	}
}

void v_init()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}

	atexit(SDL_Quit);

	SDL_SetEventFilter(event_filter);

	SDL_Surface *icon = IMG_Load("data/ne/ne_icon.png");
	if (icon) {
		SDL_WM_SetIcon(icon, NULL);
	}

	v_title("NE Development Build", "NE Dev");
}

void v_setup(int width, int height, int fullscreen)
{
	int flags = SDL_OPENGL|SDL_DOUBLEBUF|SDL_RESIZABLE;
	const SDL_VideoInfo* video = SDL_GetVideoInfo();

	if (width == 0 || height == 0) {
		width = video->current_w;
		height = video->current_h;
	}

	static int first_setup = 1;

	if (!fullscreen || first_setup) {
		vinfo.windowed_width = width;
		vinfo.windowed_height = height;
	}

	vinfo.native_width = video->current_w;
	vinfo.native_height = video->current_h;

	if (!video) {
		fprintf(stderr, "Couldn't get video information: %s\n",
				SDL_GetError());
		exit(1);
	}

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,     8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,   8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,    8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,   8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,  16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	flags |= fullscreen*SDL_FULLSCREEN;

	if ( SDL_SetVideoMode(width, height,
						  video->vfmt->BitsPerPixel, flags) == 0)
	{
		fprintf(stderr, "Couldn't set video mode: %s\n", SDL_GetError());
		exit(1);
	}

	set_vinfo(width, height, video->vfmt->BitsPerPixel, flags, fullscreen);
	first_setup = 0;
}

void v_resizecb(void (*cb)())
{
	resize_cb = cb;
}

void v_fullscreen(int changeres)
{
	/*
	SDL_WM_ToggleFullScreen(SDL_GetVideoSurface());
	*/

	vinfo.fullscreen = !vinfo.fullscreen;

	if (changeres) {
		vinfo.width = vinfo.native_width;
		vinfo.height = vinfo.native_height;
	}

	if (!vinfo.fullscreen) {
		vinfo.width = vinfo.windowed_width;
		vinfo.height = vinfo.windowed_height;
	}

	vinfo.flags ^= SDL_FULLSCREEN;

	apply_resize();
}

void v_title(const char *longname, const char *shortname)
{
	SDL_WM_SetCaption(longname, shortname);
}

void v_flip()
{
	SDL_GL_SwapBuffers();
}

double *v_unproject(float x, float y, float z)
{
	static double point[3];

	GLfloat wx, wy, wz;

	GLdouble mv[16];
	glGetDoublev(GL_MODELVIEW_MATRIX, mv);

	GLdouble proj[16];
	glGetDoublev(GL_PROJECTION_MATRIX, proj);

	GLint vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	wx = x;
	wy = vp[3] - y;
	wz = z;

	int success = gluUnProject(wx, wy, wz, mv, proj, vp, point, point+1, point+2);

	if (!success) {
		memset(point,0,3);
	}

	return point;
}

