/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef CONVERSION_H_
#define CONVERSION_H_

#include <ode/ode.h>
#include <GL/gl.h>

inline const dReal *Matrix_GL_to_ODE(const GLfloat *m)
{
	static dReal r[12];
	r[0] = m[0];
	r[4] = m[1];
	r[8] = m[2];

	r[1] = m[4];
	r[5] = m[5];
	r[9] = m[6];

	r[2] = m[8];
	r[6] = m[9];
	r[10]= m[10];

	r[3] = 0.0;
	r[7] = 0.0;
	r[11]= 0.0;

	return r;
}

#endif /* CONVERSION_H_ */
