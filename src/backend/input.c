/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "input.h"
#include <SDL/SDL.h>

//static keysym_t keys[K_LAST];
static keysym_t *keys;
static struct input_mouse mouse;

void i_init()
{
	//memset(keys,K_LAST,0);
	keys = 0;
}

void i_pump()
{
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
			case SDL_MOUSEBUTTONUP:
			if (e.button.button & SDL_BUTTON(SDL_BUTTON_WHEELUP))
				mouse.button |= SDL_BUTTON(M_WHEEL_UP);
			break;
			case SDL_MOUSEBUTTONDOWN:
			if (e.button.button & SDL_BUTTON(SDL_BUTTON_WHEELDOWN))
				mouse.button |= SDL_BUTTON(M_WHEEL_DOWN);
			break;
		}
	}

    //SDL_PumpEvents();

    keys = SDL_GetKeyState(0);

    mouse.button = SDL_GetMouseState(&mouse.x, &mouse.y);
    SDL_GetRelativeMouseState(&mouse.rel_x,&mouse.rel_y);
}

void i_clear()
{
	SDL_Event e;
	while (SDL_PollEvent(&e));
}

keysym_t* i_keystate()
{
    return keys;
}

const struct input_mouse* i_mouse()
{
    return (const struct input_mouse*)&mouse;
}

int i_mbutton(int button)
{
	return (mouse.button & M_BUTTON(button));
}

int *i_mpos()
{
	static int pos[2];
	pos[0] = mouse.x;
	pos[1] = mouse.y;
	return pos;
}

int *i_mrelpos()
{
	static int pos[2];
	pos[0] = mouse.rel_x;
	pos[1] = mouse.rel_y;
	return pos;
}

int i_mwheel()
{
	return mouse.wheel;
}
