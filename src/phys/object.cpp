/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "object.h"

#include "world.h"
#include <GL/gl.h>
#include <ode/ode.h>
#include <ode/odecpp.h>
#include <ode/odecpp_collision.h>
#include <string>
#include <sstream>
#include <vector>

Object::Object()
 : m_gravity_mag(0), m_geom(0), m_static(false), m_colfunc(0)
{
	m_body = new dBody(World::Instance()->getWorld()->id());
}

Object::~Object()
{
	delete m_body;
}

int Object::collide(Object* o)
{
	if (!m_colfunc)
	{
		return 1;
	}
	else
	{
		return m_colfunc(this,o);
	}
}

void Object::apply_matrix()
{
    const dReal *pos = m_body->getPosition();
    const dReal *rot = m_body->getRotation();

    static GLfloat matrix[16];
    matrix[0]=rot[0];
    matrix[1]=rot[4];
    matrix[2]=rot[8];
    matrix[3]=0;
    matrix[4]=rot[1];
    matrix[5]=rot[5];
    matrix[6]=rot[9];
    matrix[7]=0;
    matrix[8]=rot[2];
    matrix[9]=rot[6];
    matrix[10]=rot[10];
    matrix[11]=0;
    matrix[12]=pos[0];
    matrix[13]=pos[1];
    matrix[14]=pos[2];
    matrix[15]=1;

    glMultMatrixf (matrix);
}

void Object::init_geom()
{
	if (m_geom)
	{
		m_geom->setData(this);
		m_geom->setBody(m_body->id());
	}
}

void Object::setStatic(bool is_static)
{
	m_static = is_static;
	if (m_geom)
	{
		if (m_static)
			m_geom->setBody(0);
		else
			m_geom->setBody(m_body->id());
	}
}

bool Object::applyGravity(const Vec3f &grav)
{
	m_body->addForce(grav.x(), grav.y(), grav.z());

	float mag = grav.length();

	if (m_gravity_mag < mag)
	{
		m_gravity_mag = mag;
		m_gravity = grav;

		return true;
	}

	return false;
}

std::string &Object::serialize()
{
	std::stringstream stream;

	const dReal *p = getPosition();

	stream << "pos: ";
	stream << p[0] << ' ' << p[1] << ' ' << p[2] << ' ';

	p = getQuaternion();

	stream << "quat: ";
	stream << p[0] << ' ' << p[1] << ' ' << p[2] << ' ' << p[3] << ' ';

	static std::string out = stream.str();
	return out;
}

void Object::unserialize(std::string &s)
{
	std::stringstream stream(s);
	std::string str;
	while (stream >> str)
	{
		if (str == "pos:")
		{
			dReal x, y, z;
			stream >> x >> y >> z;
			setPosition(x,y,z);
		}
		else if (str == "quat:")
		{
			dReal q[4];
			stream >> q[0] >> q[1] >> q[2] >> q[3];
			setQuaternion(q);
		}
	}
}

void Object::setPosition(dReal x, dReal y, dReal z)
{
	if (m_static)
		m_geom->setPosition(x,y,z);
	else
		m_body->setPosition(x,y,z);
}
const dReal *Object::getPosition()
{
	if (m_static)
		return m_geom->getPosition();
	else
		return m_body->getPosition();
}

void Object::setRotation(const dMatrix3 r)
{
	if (m_static)
		m_geom->setRotation(r);
	else
		m_body->setRotation(r);
}
const dReal *Object::getRotation()
{
	if (m_static)
		return m_geom->getRotation();
	else
		return m_body->getRotation();
}

void Object::setQuaternion(const dQuaternion q)
{
	if (m_static)
		m_geom->setQuaternion(q);
	else
		m_body->setQuaternion(q);
}
const dReal *Object::getQuaternion()
{
	if (m_static)
	{
		static dReal q[4];
		m_geom->getQuaternion(q);
		return q;
	}
	else
		return m_body->getQuaternion();
}
